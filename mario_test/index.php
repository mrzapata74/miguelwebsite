<?php include("db.php")/*Requiriendo la conexion*/ ?>
<?php include("includes/header.php")/*Requiriendo Header*/ ?>

<div class="container p-4">
    <div class="row">
        <div class="col-md-3">
            <?php if (isset($_SESSION['message'])) { ?>
                <div class="alert alert-<?= $_SESSION['message_type']; ?> alert-dismissible fade show" role="alert">
                    <?= $_SESSION['message'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php  session_unset(); } ?>

            <div class="card card-body">
                <form action="save_task.php" method="POST">
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" placeholder="Titulo de la Tarea" autofocus>
                    </div>
                    <div class="form-group">
                        <textarea name="description" rows="2" class="form-control" placeholder="Descripción de la Tarea"></textarea>
                    </div>
                    <input type="submit" class="btn btn-success btn-block" name="save_task" value="Guardar Tarea">
                </form>
            </div>
        </div>


        <div class="col-md-9">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Creando Consulta PHP -->
                    
                    <?php 
                        $query = "SELECT * FROM 'clientes'";
                        $tareas_resultantes = mysqli_query($conn, $query);
                        while($fila = mysqli_fetch_array($tareas_resultantes)){ ?>
                            echo <tr>
                                echo <td><?php echo $fila['id'];?></td>
                                echo <td><?php echo $fila['nombre'];?></td>
                                echo <td><?php echo $fila['telefono'];?></td>
                                echo <td><?php echo $fila['correo'];?></td>
                                echo<td>
                                    <a href="edit_task.php?id=<?php echo $fila['id']?>" class="btn btn-secondary">
                                        <i class="fas fa-marker"></i>
                                    </a>
                                    <a href="delete_task.php?id=<?php echo $fila['id']?>" class="btn btn-danger">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                       <?php }?>
                </tbody>
            </table>

        </div>

    </div>
</div>



<?php include("includes/footer.php")/*Requiriendo Footer*/ ?>