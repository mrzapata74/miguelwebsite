<?php include("conexion.php") ?>
<!DOCTYPE>
<html>

<head>
    <title>Prueba de Conexión BD</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700,900i&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/ionicons@4.5.10-1/dist/css/ionicons.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.2/css/mdb.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="css/estilos.css">
</head>

<body>
    <h1>Datos para Mostrar de BD</h1>
    <div class="container">
        <div class="row">
            <div class="col-4">
                <form method="POST" action="registroClientes.php">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="idCte">Id</label>
                            <input class="form-control" type="text" name="IdCte" id="ideCte">
                        </div>
                        <div class="form-group">
                            <label for="nombreCte">Nombre</label>
                            <input class="form-control" type="text" name="nombre" id="nombreCte">
                        </div>
                        <div class="form-group">
                            <label for="celCte">Telefono</label>
                            <input class="form-control" type="text" name="telefono" id="celCte">
                        </div>
                        <div class="form-group">
                            <label for="correoCte">Correo</label>
                            <input class="form-control" type="text" name="correo" id="correoCte">
                        </div>



                        <!-- 
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                    </div>
                    <!--                     <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div> -->
                    <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
                    <button type="submit" class="btn btn-secondary" name="consultar">Consultar</button>
                </form>
                <?php
                    include('conexion.php');
                    $nombre = $_POST['nombre'];
                    $telefono = $_POST['telefono'];
                    $correo = $_POST['correo'];

                    if(isset($_POST['consultar'])){
                        $id = $_POST['IdCte'];

                        $resultado = mysqli_query($conexion, "SELECT * FROM $tabla_Cte WHERE id= $id");
                        while($consulta = mysqli_fetch_array($resultado)){
                            echo $consulta['id']."<br>";
                            echo $consulta['nombre']."<br>";
                            echo $consulta['telefono']."<br>";
                            echo $consulta['correo']."<br>";
                        }
                        header('Location: mario.php');

                    }
                
                
                ?>
            </div>
            <div class="col-8">

                <table class="table table-striped">
                    <tr>
                        <th>id</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th>Acciones</th>
                    </tr>
                    <?php
                    while ($lista_clientes = mysqli_fetch_array($ejecutar_sentencia)) { ?>
                        <tr>
                            <td><?php echo $consulta['id']; ?></td>
                            <td><?php echo $consulta['nombre']; ?></td>
                            <td><?php echo $consulta['telefono']; ?></td>
                            <td><?php echo $consulta['correo']; ?></td>
                            <td>
                                <a href="edit_task.php?id=<?php echo $consulta['id'] ?>" class="btn btn-secondary">
                                    <i class="fas fa-marker"></i>
                                </a>
                                <a href="delete_task.php?id=<?php echo $consulta['id'] ?>" class="btn btn-danger">
                                    <i class="far fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>

</body>

</html>